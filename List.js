
import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

/*const list = {
    id,
    userId,
    title,
    body
}*/

class List extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            listTitle: this.props.title,
            data: [],
            loading: false
        }
    }

    componentWillMount() {
        // getList
        this.getList();
    }

    getList() {
        let apiUrl = 'https://jsonplaceholder.typicode.com/posts';
        fetch(apiUrl)
            .then(res => res.json)
            .then(res =>
                this.setState({
                    data: res
                })
            );
    }

    render() {
        let listTitle = this.state.listTitle;
        let items = this.state.data;
        return (
            <View>
                <View style={styles.container}>
                    <Text>{listTitle}</Text>
                </View>
                <FlatList
                    data={items}
                    renderItem={({ item }) => <View style={styles.itemBox} key={item.id}><Text style={styles.user}>by: {item.userId}</Text><Text style={styles.title}>{item.title}</Text><Text style={styles.body}>{item.body}</Text></View>}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    list: {
        flex: 1
    },
    itemBox: {
        flex: 1,
        padding: 10,
        margin: 10
    },
    user: {
        fontSize: 11,
        color: '#ccc'
    },
    title: {
        fontSize: 18
    },
    body: {

    }

});

export default List;
